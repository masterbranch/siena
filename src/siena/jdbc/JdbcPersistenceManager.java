/*
 * Copyright 2008 Alberto Gimeno <gimenete at gmail.com>
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package siena.jdbc;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import siena.ClassInfo;
import siena.Generator;
import siena.Id;
import siena.Json;
import siena.PersistenceManager;
import siena.Query;
import siena.SienaException;
import siena.Util;
import siena.embed.Embedded;
import siena.embed.JsonSerializer;

public class JdbcPersistenceManager implements PersistenceManager {
	
	private Map<Class<?>, JdbcClassInfo> infoClasses;
	
	private ConnectionManager connectionManager;
	
	public JdbcPersistenceManager() {
		infoClasses = new ConcurrentHashMap<Class<?>, JdbcClassInfo>();
	}

	public JdbcPersistenceManager(ConnectionManager connectionManager, Class<?> listener) {
		this();
		this.connectionManager = connectionManager;
	}
	
	public void init(Properties p) {
		String cm = p.getProperty("transactions");
		if(cm != null) {
			try {
				connectionManager = (ConnectionManager) Class.forName(cm).newInstance();
			} catch (Exception e) {
				throw new SienaException(e);
			}
		} else {
			connectionManager = new ThreadedConnectionManager();
		}

		connectionManager.init(p);
	}

	public Connection getConnection() throws SQLException {
		return connectionManager.getConnection();
	}

	public <T> Query<T> createQuery(Class<T> clazz) {
		return new JdbcQuery<T>(clazz, this);
	}

	public JdbcClassInfo getClassInfo(Class<?> clazz) {
		JdbcClassInfo ci = infoClasses.get(clazz);
		if(ci == null) {
			ci = new JdbcClassInfo(clazz);
			infoClasses.put(clazz, ci);
		}
		return ci;
	}

	private Object readField(Object object, Field field) {
		field.setAccessible(true);
		try {
			return field.get(object);
		} catch (Exception e) {
			throw new SienaException(e);
		}
	}

	public void delete(Object obj) {
		JdbcClassInfo classInfo = getClassInfo(obj.getClass());

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = getConnection();
			ps = connection.prepareStatement(classInfo.deleteSQL);
			addParameters(obj, classInfo.keys, ps, 1);
			int n = ps.executeUpdate();
			if(n == 0) {
				throw new SienaException("No updated rows");
			}
			if(n > 1) {
				throw new SienaException(n+" rows deleted");
			}
		} catch(SQLException e) {
			throw new SienaException(e);
		} finally {
			closeStatement(ps);
			closeConnection();
		}
	}

	public void get(Object obj) {
		JdbcClassInfo classInfo = getClassInfo(obj.getClass());

		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			connection = getConnection();
			ps = connection.prepareStatement(classInfo.selectSQL);
			addParameters(obj, classInfo.keys, ps, 1);
			rs = ps.executeQuery();
			if(rs.next()) {
				mapObject(obj, rs);
			} else {
				throw new SienaException("No such object");
			}
		} catch(SQLException e) {
			throw new SienaException(e);
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
			closeConnection();
		}
	}

	public int addParameters(Object obj, List<Field> fields, PreparedStatement ps, int i) throws SQLException {
		for (Field field : fields) {
			Class<?> type = field.getType();
			if(ClassInfo.isModel(type)) {
				JdbcClassInfo ci = getClassInfo(type);
				Object rel = readField(obj, field);
				for(Field f : ci.keys) {
					if(rel != null) {
						Object value = readField(rel, f);
						if(value instanceof Json)
							value = ((Json)value).toString();
						ps.setObject(i++, value);
					} else {
						ps.setObject(i++, null);
					}
				}
			} else {
				Object value = readField(obj, field);
				if(value instanceof Json)
					value = ((Json)value).toString();
				else if(field.getAnnotation(Embedded.class) != null)
					value = JsonSerializer.serialize(value).toString();
				ps.setObject(i++, value);
			}
		}
		return i;
	}

	public void insert(Object obj) {
		JdbcClassInfo classInfo = getClassInfo(obj.getClass());

		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet gk = null;
		try {
			connection = getConnection();
			if(!classInfo.generatedKeys.isEmpty()) {
				ps = connection.prepareStatement(classInfo.insertSQL, Statement.RETURN_GENERATED_KEYS);
			} else {
				ps = connection.prepareStatement(classInfo.insertSQL);
			}
			for (Field field : classInfo.keys) {
				Id id = field.getAnnotation(Id.class);
				if(id.value() == Generator.UUID) {
					field.set(obj, UUID.randomUUID().toString());
				}
			}
			// TODO: implement primary key generation: SEQUENCE
			addParameters(obj, classInfo.insertFields, ps, 1);
			ps.executeUpdate();
			if(!classInfo.generatedKeys.isEmpty()) {
				gk = ps.getGeneratedKeys();
				if(!gk.next())
					throw new SienaException("No such generated keys");
				int i = 1;
				for (Field field : classInfo.generatedKeys) {
					field.setAccessible(true);
					Util.setFromObject(obj, field, gk.getObject(i));
					// field.set(obj, gk.getObject(i));
					i++;
				}
			}
		} catch(SienaException e) {
			throw e;
		} catch(Exception e) {
			throw new SienaException(e);
		} finally {
			closeResultSet(gk);
			closeStatement(ps);
			closeConnection();
		}
	}

	public void update(Object obj) {
		JdbcClassInfo classInfo = getClassInfo(obj.getClass());

		Connection connection = null;
		PreparedStatement ps = null;
		try {
			connection = getConnection();
			ps = connection.prepareStatement(classInfo.updateSQL);
			int i = 1;
			i = addParameters(obj, classInfo.updateFields, ps, i);
			addParameters(obj, classInfo.keys, ps, i);
			ps.executeUpdate();
		} catch(SQLException e) {
			throw new SienaException(e);
		} finally {
			closeStatement(ps);
			closeConnection();
		}
	}

	public String[] getColumnNames(Class<?> clazz, String field) {
		try {
			return ClassInfo.getColumnNames(clazz.getDeclaredField(field));
		} catch (Exception e) {
			throw new SienaException(e);
		}
	}

	public <T> T mapObject(Class<T> clazz, ResultSet rs) {
		try {
			T obj = clazz.newInstance();
			mapObject(obj, rs);
			return obj;
		} catch(SienaException e) {
			throw e;
		} catch(Exception e) {
			throw new SienaException(e);
		}
	}

	public void mapObject(Object obj, ResultSet rs) {
		Class<?> clazz = obj.getClass();
		for (Field field : getClassInfo(clazz).allFields) {
			mapField(obj, field, rs);
		}
	}

	public <T> List<T> mapList(Class<T> clazz, ResultSet rs) {
		try {
			List<T> objects = new ArrayList<T>();
			while(rs.next()) {
				objects.add(mapObject(clazz, rs));
			}
			return objects;
		} catch(SienaException e) {
			throw e;
		} catch(Exception e) {
			throw new SienaException(e);
		}
	}

	private void mapField(Object obj, Field field, ResultSet rs) {
		Class<?> type = field.getType();
		field.setAccessible(true);
		try {
			if(ClassInfo.isModel(type)) {
				String[] fks = ClassInfo.getColumnNames(field);
				Object rel = type.newInstance();
				JdbcClassInfo classInfo = getClassInfo(type);
				boolean none = false;
				int i = 0;
				checkForeignKeyMapping(classInfo.keys, fks, obj.getClass(), field);
				for(Field f : classInfo.keys) {
					Object o = rs.getObject(fks[i++]);
					if(o == null) {
						none = true;
						break;
					}
					Util.setFromObject(rel, f, o);
					// f.set(rel, o);
				}
				if(!none)
					field.set(obj, rel);
			} else {
				Object val = rs.getObject(ClassInfo.getColumnNames(field)[0]);
				Util.setFromObject(obj, field, val);
				// field.set(obj, val);
			}
		} catch (SienaException e) {
			throw e;
		} catch (Exception e) {
			throw new SienaException(e);
		}
	}

	public void checkForeignKeyMapping(List<Field> keys, String[] columns, Class<?> clazz, Field field) {
		if (keys.size() != columns.length) {
			throw new SienaException("Bad mapping for field '"+field.getName()+"'. " +
					"Related class "+field.getType().getName()+" has "+keys.size()+" primary keys, " +
					"but '"+clazz.getName()+"' only has mappings for "+columns.length+" foreign keys");
		}
	}

	public void closeStatement(Statement st) {
		if(st == null) return;
		try {
			st.close();
		} catch (SQLException e) {
			throw new SienaException(e);
		}
	}

	public void closeResultSet(ResultSet rs) {
		if(rs == null) return;
		try {
			rs.close();
		} catch (SQLException e) {
			throw new SienaException(e);
		}
	}

	public void beginTransaction(int isolationLevel) {
		connectionManager.beginTransaction(isolationLevel);
	}

	public void commitTransaction() {
		connectionManager.commitTransaction();
	}

	public void rollbackTransaction() {
		connectionManager.rollbackTransaction();
	}
	
	public void closeConnection() {
		connectionManager.closeConnection();
	}


	class JdbcClassInfo {
		public String tableName;
		public String insertSQL;
		public String updateSQL;
		public String deleteSQL;
		public String selectSQL;
		public String baseSelectSQL;

		public List<Field> keys = null;
		public List<Field> insertFields = null;
		public List<Field> updateFields = null;
		public List<Field> generatedKeys = null;
		public List<Field> allFields = null;

		public JdbcClassInfo(Class<?> clazz) {
			ClassInfo info = ClassInfo.getClassInfo(clazz);
			
			keys = info.keys;
			insertFields = info.insertFields;
			updateFields = info.updateFields;
			generatedKeys = info.generatedKeys;
			allFields = info.allFields;
			tableName = info.tableName;
			
			List<String> keyColumns = new ArrayList<String>();
			List<String> insertColumns = new ArrayList<String>();
			List<String> updateColumns = new ArrayList<String>();
			List<String> allColumns = new ArrayList<String>();

			calculateColumns(insertFields, insertColumns, "");
			calculateColumns(updateFields, updateColumns, "=?");
			calculateColumns(keys, keyColumns, "=?");
			calculateColumns(allFields, allColumns, "");

			deleteSQL = "DELETE FROM "+tableName+" WHERE "+Util.join(keyColumns, " AND ");

			String[] is = new String[insertColumns.size()];
			Arrays.fill(is, "?");
			insertSQL = "INSERT INTO "+tableName+" ("+Util.join(insertColumns, ", ")+") VALUES("+Util.join(Arrays.asList(is), ", ")+")";

			updateSQL = "UPDATE "+tableName+" SET ";
			updateSQL += Util.join(updateColumns, ", ");
			updateSQL += " WHERE ";
			updateSQL += Util.join(keyColumns, " AND ");

			baseSelectSQL = "SELECT "+Util.join(allColumns, ", ")+" FROM "+tableName;

			selectSQL = baseSelectSQL+" WHERE "+Util.join(keyColumns, " AND ");
		}
		
		private void calculateColumns(List<Field> fields, List<String> columns, String suffix) {
			for (Field field : fields) {
				String[] columnNames = ClassInfo.getColumnNames(field);
				for (String columnName : columnNames) {
					columns.add(columnName+suffix);
				}
			}
		}

	}


	public void setConnectionManager(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

}
